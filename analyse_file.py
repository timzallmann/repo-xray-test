import json
from neo4j import GraphDatabase
from tree_sitter import Language, Parser, Tree

from langchain.schema import HumanMessage, AIMessage
from codeutils import get_code_from_md_block
from code_prompt import get_code_template
from langchain.chat_models import ChatAnthropic

JS_LANGUAGE = Language('build/my-languages.so', 'javascript')

parser = Parser()
parser.set_language(JS_LANGUAGE)

URI = "neo4j://localhost"
AUTH = ("neo4j", "bus-club-veer-simian")

with GraphDatabase.driver(URI, auth=AUTH) as driver:
    driver.verify_connectivity()

def add_tree_info(code_info):
  code_bytes = bytes(code_info["code"], "utf8")

  def getNodeText(selected_node):
    return code_bytes[selected_node.start_byte:selected_node.end_byte].decode('utf8')

  code_tree = parser.parse(code_bytes)

  query = JS_LANGUAGE.query("""
  (import_statement) @import
  """)

  captures = query.captures(code_tree.root_node)

  for capture in captures:
    selected_node = capture[0]
    import_source = getNodeText(selected_node.child_by_field_name("source"))
    import_source = import_source.replace("'", "")
    print("Import : " + import_source)
    print(getNodeText(selected_node))

    new_import = {
      "source": import_source,
      "elements":[]
    }

    print(new_import)

    if (import_source.__contains__("/") == -1):
      code_info["internal_imports"].append(new_import)      
    else:
      code_info["library_imports"].append(new_import)

  query = JS_LANGUAGE.query("""
  (
    (comment)* @doc
    .
    (method_definition
      name: (property_identifier) @name) @definition.method
    (#not-eq? @name "constructor")
    (#strip! @doc "^[\\s\\*/]+|^[\\s\\*/]$")
    (#select-adjacent! @doc @definition.method)
  )

  (
    (comment)* @doc
    .
    [
      (class
        name: (_) @name)
      (class_declaration
        name: (_) @name)
    ] @definition.class
    (#strip! @doc "^[\\s\\*/]+|^[\\s\\*/]$")
    (#select-adjacent! @doc @definition.class)
  )

  (
    (comment)* @doc
    .
    [
      (function
        name: (identifier) @name)
      (function_declaration
        name: (identifier) @name)
      (generator_function
        name: (identifier) @name)
      (generator_function_declaration
        name: (identifier) @name)
    ] @definition.function
    (#strip! @doc "^[\\s\\*/]+|^[\\s\\*/]$")
    (#select-adjacent! @doc @definition.function)
  )

  (
    (comment)* @doc
    .
    (lexical_declaration
      (variable_declarator
        name: (identifier) @name
        value: [(arrow_function) (function)]) @definition.function)
    (#strip! @doc "^[\\s\\*/]+|^[\\s\\*/]$")
    (#select-adjacent! @doc @definition.function)
  )

  (
    (comment)* @doc
    .
    (variable_declaration
      (variable_declarator
        name: (identifier) @name
        value: [(arrow_function) (function)]) @definition.function)
    (#strip! @doc "^[\\s\\*/]+|^[\\s\\*/]$")
    (#select-adjacent! @doc @definition.function)
  )

  (assignment_expression
    left: [
      (identifier) @name
      (member_expression
        property: (property_identifier) @name)
    ]
    right: [(arrow_function) (function)]
  ) @definition.function

  """)

  captures = query.captures(code_tree.root_node)

  for capture in captures:
    print(capture)
    print(getNodeText(capture[0]))
    if (capture[1]=='name'):
      print(getNodeText(capture[0]))
      prev_type = prev_node[1]
      if (prev_type=='definition.function' or prev_type=='definition.method'):
        code_info["tree_info"]["functions"].append(getNodeText(capture[0]))
      elif (prev_type=='definition.class'):
        code_info["tree_info"]["classes"].append(getNodeText(capture[0]))
    prev_node = capture

def add_llm_info(code_info):
  token_target = 16000

  llm_anthropic = ChatAnthropic(model="claude-2", temperature="0.1", max_tokens_to_sample=token_target, verbose=True)

  analyse_template = get_code_template(code_info)
  
  start_message = "{\"file_summary\":"

  messages = [
      HumanMessage(content=analyse_template),
      AIMessage(content=start_message)
  ]

  print("-------------")
  print(analyse_template)
  print("-------------")
  
  resp = llm_anthropic(messages)

  result_code = start_message + resp.content
  result_code = result_code.replace("```", "")

  print('Anthropic Response:')
  print(result_code)
  
  firstValue = result_code.index("{")
  lastValue = len(result_code) - result_code[::-1].index("}")
  jsonString = result_code[firstValue:lastValue]

  llm_info = json.loads(jsonString)

  with open("test_llm_code.json", "w") as write_file:
    json.dump(llm_info, write_file)

  with open("test_llm_code.json", "r") as read_file:
    llm_info = json.load(read_file)

  code_info["file_summary"] = llm_info["file_summary"]

  code_info["classes"] = llm_info["classes"]
  code_info["functions"] = llm_info["functions"]

  code_info["language_specific"] = llm_info["language_specific"]
  code_info["project_specific"] = llm_info["project_specific"]

  return code_info


def analyse_file(base_path, file_path):
  with open(base_path + file_path, 'r') as file:
    code = file.read()

  code_info = {
    "name": file_path[file_path.rindex("/") + 1:],
    "file_path": file_path, 
    "directory_path": file_path[0:file_path.rindex("/") + 1],
    "characters": len(code),
    "lines": code.count('\n'),
    "code": code,
    "language_specific":[],
    "project_specific":[],
    "code_tree":[],
    "library_imports": [],
    "internal_imports":[],
    "tree_info": {
      "classes":[],
      "functions":[],
    }
  }

  add_tree_info(code_info)

  print(code_info["tree_info"]["functions"])

  add_llm_info(code_info)

  with open("test_code.json", "w") as write_file:
    json.dump(code_info, write_file)

  return code_info

def save_file_to_db(code_info):
  # Add file node to graph database
  summary = driver.execute_query(
      "MERGE (:File {name: $name, file_path: $file_path, directory_path: $directory_path, characters: $characters, lines: $lines, file_summary: $file_summary})",
      name=code_info["name"],
      file_path=code_info["file_path"],
      directory_path=code_info["directory_path"],
      characters=code_info["characters"],
      lines=code_info["lines"],
      file_summary=code_info["file_summary"],
      database_="neo4j",
  ).summary
  
  print("Created {nodes_created} nodes in {time} ms.".format(
    nodes_created=summary.counters.nodes_created,
    time=summary.result_available_after
  ))

  for class_info in code_info["classes"]:
    # Add class node to graph database
    summary = driver.execute_query(
        "MERGE (:Class {name: $name, file_path: $file_path, description: $description})",
        name=class_info["class_name"],
        file_path=code_info["file_path"],
        description=class_info["class_description"],
        database_="neo4j",
    ).summary

    print("Created {nodes_created} nodes in {time} ms.".format(
      nodes_created=summary.counters.nodes_created,
      time=summary.result_available_after
    ))

    summary = driver.execute_query("""
      MATCH (f:File {file_path: $file_path})
      MATCH (c:Class {name: $class_name, file_path: $file_path})
      MERGE (c)-[:LOCATED_IN]->(f)
      """, file_path=code_info["file_path"], class_name=class_info["class_name"],
      database_="neo4j",
    )

  for function_info in code_info["functions"]:
    # Add class node to graph database
    summary = driver.execute_query(
        "MERGE (:Function {name: $name, file_path: $file_path, description: $description})",
        name=function_info["function_name"],
        file_path=code_info["file_path"],
        description=function_info["function_description"],
        database_="neo4j",
    ).summary

    print("Created {nodes_created} nodes in {time} ms.".format(
      nodes_created=summary.counters.nodes_created,
      time=summary.result_available_after
    ))

    summary = driver.execute_query("""
      MATCH (f:File {file_path: $file_path})
      MATCH (fu:Function {name: $function_name, file_path: $file_path})
      MERGE (fu)-[:LOCATED_IN]->(f)
      """, file_path=code_info["file_path"], function_name=function_info["function_name"],
      database_="neo4j",
    )

#
# Analyse files
#
test_repo_path = "../gitlab-development-kit/gitlab/"

# test_file = analyse_file(test_repo_path, "app/assets/javascripts/lib/utils/vue3compat/vue_router.js")
test_file = analyse_file(test_repo_path, "app/assets/javascripts/lib/utils/url_utility.js")
print(test_file)

# For testing delete all nodes
driver.execute_query("MATCH (n) DETACH DELETE n")

# Save test file to DB
save_file_to_db(test_file)

# driver.close()
