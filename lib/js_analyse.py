def get_js_prompt(file_path, code, function_list, class_list):
    print("JS File Prompt")
    analyse_template = f"""
        As a JavaScript engineer, your task is to provide a summary of the code file located at '{file_path}' and each individual function within the file. 
       
        <code>
        {code}
        </code>

        {'Please analyse the following functions and methods in the file:' if function_list else ''}
        {function_list}

        {'Add descriptions for the following classes: ' if class_list else ''}
        {class_list}
        
        Format the summary as valid JSON in the following way:
        {{        
         \"file_summary\": \"(Description of the code in the file)\",
         \"language\": \"(language)\",
         \"contains_tests\": \"(boolean that shows if this file contains tests or specs)\",         
         \"language_specific\": {{
           \"js_version\":\"(Javascript version, ES6 for example)\",
           \"type_of_js\":\"(Frontend, Node or Both)\"
        }},
        \"project_specific\": {{
           \"uses_jquery\":\"(boolean if this file uses jquery)\",
           \"es7_compatible\":\"(Is the file ES7 compatible)\"
        }},
        \"functions\": [
          {{
              \"function_name\":\"(Name of the function)\",
              \"function_description\":\"(Description of the function in maximum 5 sentences)\",
              \"function_parameters\": [
                  {{
                      \"name\":\"(Name of the parameter)\",
                      \"description\":\"(description of the parameter)\",
                      \"parameter_type\":"(javascript type of the parameter - string, object, number, boolean, bigint, symbol or undefined)\"
                  }}
              ]
          }}
        ],
        \"classes\": [
          {{
              \"class_name\":\"(Name of the class)\",
              \"class_description\":\"(Description of the class in maximum 5 sentences)\",
          }}
        ]
        }}

        Please ensure that your summary accurately describes the content and purpose of the file and each individual function within it. The summary should be formatted as valid JSON and include details such as the language, version of JavaScript, and type of file. Additionally, please provide information on any library or file imports, public exports, and function parameters.
        """
    return analyse_template
