def get_vue_prompt(file_path, code):
    print("Vue File Prompt")
    analyse_template = f"""
        As a JavaScript engineer, your task is to provide a summary of the Vue file located at '{file_path}' and each individual function within the file. 
       
        <code>
        {code}
        </code>
        
         Please format the summary as valid JSON in the following way:
        {{        
         \"file_summary\": \"(Description of the file content)\",
         \"language\": \"(language)\",
         \"language_specific\": {{
           \"js_version\":\"(Javascript version, ES6 for example)\",
           \"vue_version\":\"(Vue version, 2 or 3)\",
           \"name_of_component\":\"(The name of the Vue component)\",
           \"props\": [
                  {{
                      \"name\":\"(Name of the Vue props)\",
                      \"description\":\"(description of the property)\",
                      \"parameter_type\":"(javascript type of the property - string, object, number, boolean, bigint, symbol or undefined)\"
                  }}
              ],
            \"computed_properties\": [ // List all Vue computed properties under the property computed
                  {{
                      \"name\":\"(name of the property)\",
                      \"description\":\"(description of the computed property)\"
                  }}
              ],
            "props\": [(All components used by this Vue Component)],
        }},
        \"project_specific\": {{
           \"uses_vuex\":\"(boolean if this file uses vuex)\",
           \"es7_compatible\":\"(Is the file ES7 compatible)\"
        }},
        \"library_imports\": [\"(libraries imported)\"],
        \"file_imports\": [\"(file paths imported)\"],
        \"public_exports\": [\"(classes, functions and variables exported)\"],
        \"functions\": [
          {{
              \"function_name\":\"(Name of the function)\",
              \"function_description\":\"(Description of the function in maximum 5 sentences)\",
              \"function_parameters\": [
                  {{
                      \"name\":\"(Name of the parameter)\",
                      \"description\":\"(description of the parameter)\",
                      \"parameter_type\":"(javascript type of the parameter - string, object, number, boolean, bigint, symbol or undefined)\"
                  }}
              ]
          }}
        ]
        }}

        Please ensure that your summary accurately describes the content and purpose of the file and each individual function within it. The summary should be formatted as valid JSON and include details such as the language, version of JavaScript, and type of file. Additionally, please provide information on any library or file imports, and function parameters.
        """
    return analyse_template
